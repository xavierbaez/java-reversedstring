public class Main {
    public static void main(String[] args) {
        String str = "123456789abcdefg";
        System.out.println("Original String: " + str);
        char currentCharacter;
        String newReversedString = "";
        for (int i = 0; i< str.length(); i++) {
            // extracting character
            currentCharacter = str.charAt(i);
            // add character in front of the string 
            newReversedString = currentCharacter + newReversedString;
        }
        System.out.println("Reversed string: " + newReversedString);
    }
}